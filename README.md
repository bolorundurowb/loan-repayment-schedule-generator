# loan-repayment-schedule-generator

A simple command line tool written in C# and using a number of .NET tooling improvemenets (AOT, Binary Compression and Trimming) to generate a small, functional loan repayment calculator. It takes in a number of parameters and generates a CSV with the repayment details dispayed.

## Usage

To use the tool, change direactpries to where the downloaded executable is and run the following:

```bash
loan-schedule-generator.exe --help
```

That lists the parameters required to use the tool.