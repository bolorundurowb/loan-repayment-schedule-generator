﻿// See https://aka.ms/new-console-template for more information

using System.CommandLine;
using System.Text;

const string dataFormat = "{0}, {1}, {2}, {3}, {4}, {5}";
var rootCommand = new RootCommand();

var interestRateArg = new Argument<decimal>("interest-rate");
rootCommand.AddArgument(interestRateArg);

var tenorArg = new Argument<int>("remaining-tenor-in-months");
rootCommand.AddArgument(tenorArg);

var principalArg = new Argument<decimal>("remaining-principal");
rootCommand.AddArgument(principalArg);

var startDateArg = new Argument<DateOnly>("start-date", () => DateOnly.FromDateTime(DateTime.Now));
rootCommand.AddArgument(startDateArg);

var repaymentDayArg = new Argument<int>("monthly-repayment-day", () => 28);
rootCommand.AddArgument(repaymentDayArg);

var fixedRepaymentOption =
    new Option<decimal?>(new[] { "--fixed-repayment", "-fr" }, "If you want a fixed repayment amount every month");
rootCommand.AddOption(fixedRepaymentOption);

var fixedRepaymentStartOption = new Option<DateOnly?>(new[] { "--fixed-repayment-start-date", "-frs" },
    "If you do not want the fixed repayment to be effected from the start of the loan");
rootCommand.AddOption(fixedRepaymentStartOption);

var monthlyPrincipalRepaymentOption = new Option<decimal?>(new[] { "--monthly-principal-repayment", "-mpr" },
    "Allows for more precise computations");
rootCommand.AddOption(monthlyPrincipalRepaymentOption);

rootCommand.SetHandler((interestRate, tenorInMonths, principalAmount, startDate, monthlyRepaymentDay,
        fixedPrincipalRepayment, fixedRepaymentStartDate, monthlyPrincipalRepayment) =>
    {
        fixedRepaymentStartDate ??= DateOnly.MinValue;
        var builder = new StringBuilder();
        builder.AppendLine(string.Format(dataFormat, "Interest Rate", interestRate, string.Empty, string.Empty,
            string.Empty, string.Empty));
        AddBlankLine(builder);

        builder.AppendLine(string.Format(dataFormat, "Date", "Expected Principal Repayment",
            "Additional Principal Repayment", "Interest Paid", "Total Due", "Principal Outstanding"));

        decimal totalRepayment = 0;
        decimal totalPrincipalRepayment = 0;
        decimal totalInterestRepayment = 0;

        while (tenorInMonths > 0)
        {
            var repaymentDate = startDate.Day >= monthlyRepaymentDay
                ? GetDateNextMonth(startDate, monthlyRepaymentDay)
                : new DateOnly(startDate.Year, startDate.Month, monthlyRepaymentDay);

            var elapsedDays = repaymentDate.DayNumber - startDate.DayNumber;
            var interestDue = Math.Round((principalAmount * interestRate * elapsedDays) / (100m * 365), 2);
            var principalDue = monthlyPrincipalRepayment ?? Math.Round((principalAmount / tenorInMonths), 2);
            var additionalPrincipalDue = fixedPrincipalRepayment.HasValue && fixedRepaymentStartDate < repaymentDate
                ? fixedPrincipalRepayment.Value - (principalDue + interestDue)
                : 0;

            if (principalAmount < principalDue)
            {
                principalDue = principalAmount;
                additionalPrincipalDue = 0;
            }

            var totalPrincipalDue = principalDue + additionalPrincipalDue;
            var totalDue = interestDue + totalPrincipalDue;
            var outstandingPrincipal = principalAmount - totalPrincipalDue;

            builder.AppendLine(string.Format(
                    dataFormat,
                    repaymentDate.ToString("dd-MM-yyyy"),
                    principalDue.ToString("#.##"),
                    additionalPrincipalDue.ToString("#.##"),
                    interestDue.ToString("#.##"),
                    totalDue.ToString("#.##"),
                    outstandingPrincipal.ToString("#.##")
                )
            );

            totalRepayment += totalDue;
            totalInterestRepayment += interestDue;
            totalPrincipalRepayment += totalPrincipalDue;

            startDate = repaymentDate;
            principalAmount = outstandingPrincipal;
            tenorInMonths -= 1;
        }

        AddBlankLine(builder);
        AddBlankLine(builder);
        builder.AppendLine(string.Format(dataFormat, "TOTAL:", totalRepayment, totalPrincipalRepayment,
            totalInterestRepayment, string.Empty, string.Empty));

        File.WriteAllText($"./{Guid.NewGuid()}-repayment-schedule.csv", builder.ToString());
        Console.WriteLine("Done generating repayment schedule");
    }, interestRateArg, tenorArg, principalArg, startDateArg, repaymentDayArg, fixedRepaymentOption,
    fixedRepaymentStartOption, monthlyPrincipalRepaymentOption);

await rootCommand.InvokeAsync(args);

DateOnly GetDateNextMonth(DateOnly dateOnly, int? dayOverride = null)
{
    var year = dateOnly.Year;
    var day = dayOverride ?? dateOnly.Day;
    int month;

    if (dateOnly.Month == 12)
    {
        month = 1;
        year += 1;
    }
    else
    {
        month = dateOnly.Month + 1;
    }

    return new DateOnly(year, month, day);
}

void AddBlankLine(StringBuilder builder)
{
    builder.AppendLine(string.Format(dataFormat, string.Empty, string.Empty, string.Empty, string.Empty,
        string.Empty, string.Empty));
}
